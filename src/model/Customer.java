package model;

import java.util.Objects;

public class Customer {
    private String id;

    private OriginSourceEnum originSource;

    private StatusEnum status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public OriginSourceEnum getOriginSource() {
        return originSource;
    }

    public void setOriginSource(OriginSourceEnum originSource) {
        this.originSource = originSource;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Customer customer = (Customer) o;
        return Objects.equals(this.id, customer.id) &&
                Objects.equals(this.originSource, customer.originSource) &&
                Objects.equals(this.status, customer.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, originSource, status);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Customer {\n");
        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    originSource: ").append(toIndentedString(originSource)).append("\n");
        sb.append("    status: ").append(toIndentedString(status)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }

    public static Builder newBuilder() {
        return new Customer().new Builder();
    }

    public class Builder {

        private Builder() {
        }

        public Customer.Builder setId(String id) {
            Customer.this.id = id;

            return this;
        }

        public Customer.Builder setOriginSource(OriginSourceEnum originSource) {
            Customer.this.originSource = originSource;

            return this;
        }

        public Customer.Builder setStatus(StatusEnum status) {
            Customer.this.status = status;

            return this;
        }

        public Customer build() {
            return Customer.this;
        }

    }
}
