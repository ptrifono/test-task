package model;

public enum OriginSourceEnum {
    FN("FN"),

    MF("MF"),

    MH("MH");

    private String value;

    OriginSourceEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public String toString() {
        return String.valueOf(value);
    }

    public static OriginSourceEnum fromValue(String value) {
        for (OriginSourceEnum b : OriginSourceEnum.values()) {
            if (b.value.equals(value)) {
                return b;
            }
        }
        throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
}
