package model;

import java.util.List;
import java.util.Objects;

public class CdpPartyResult {
    private String id;
    private List<CrmPartyRef> crmPartyRefs;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<CrmPartyRef> getCrmPartyRefs() {
        return crmPartyRefs;
    }

    public void setCrmPartyRefs(List<CrmPartyRef> crmPartyRefs) {
        this.crmPartyRefs = crmPartyRefs;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CdpPartyResult cdpPartyAndCrmRefsDetails = (CdpPartyResult) o;
        return Objects.equals(this.id, cdpPartyAndCrmRefsDetails.id) &&
                Objects.equals(this.crmPartyRefs, cdpPartyAndCrmRefsDetails.crmPartyRefs);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, crmPartyRefs);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class model.CdpPartyResult {\n");
        sb.append("    id: ")
                .append(toIndentedString(id))
                .append("\n");
        sb.append("    crmPartyRefs: ")
                .append(toIndentedString(crmPartyRefs))
                .append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString()
                .replace("\n", "\n    ");
    }

    public static Builder newBuilder() {
        return new CdpPartyResult().new Builder();
    }

    public class Builder {

        private Builder() {
        }

        public Builder setId(String id) {
            CdpPartyResult.this.id = id;

            return this;
        }

        public Builder setCrmPartyRefs(List<CrmPartyRef> crmPartyRefs) {
            CdpPartyResult.this.crmPartyRefs = crmPartyRefs;

            return this;
        }

        public CdpPartyResult build() {
            return CdpPartyResult.this;
        }

    }
}
