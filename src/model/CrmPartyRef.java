package model;

import java.util.List;
import java.util.Objects;

public class CrmPartyRef {
    private String id;
    private Customer customer;
    private List<RelatedPartyRef> partyRelationships;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<RelatedPartyRef> getPartyRelationships() {
        return partyRelationships;
    }

    public void setPartyRelationships(List<RelatedPartyRef> partyRelationships) {
        this.partyRelationships = partyRelationships;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CrmPartyRef crmPartyRef = (CrmPartyRef) o;
        return Objects.equals(this.id, crmPartyRef.id) &&
                Objects.equals(this.customer, crmPartyRef.customer) &&
                Objects.equals(this.partyRelationships, crmPartyRef.partyRelationships);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, customer, partyRelationships);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class model.CrmPartyRef {\n");
        sb.append("    id: ")
                .append(toIndentedString(id))
                .append("\n");
        sb.append("    customer: ")
                .append(toIndentedString(customer))
                .append("\n");
        sb.append("    partyRelationships: ")
                .append(toIndentedString(partyRelationships))
                .append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString()
                .replace("\n", "\n    ");
    }

    public static Builder newBuilder() {
        return new CrmPartyRef().new Builder();
    }

    public class Builder {

        private Builder() {
        }

        public CrmPartyRef.Builder setId(String id) {
            CrmPartyRef.this.id = id;

            return this;
        }

        public CrmPartyRef.Builder setCustomer(Customer customer) {
            CrmPartyRef.this.customer = customer;

            return this;
        }

        public CrmPartyRef.Builder setPartyRelationships(List<RelatedPartyRef> partyRelationships) {
            CrmPartyRef.this.partyRelationships = partyRelationships;

            return this;
        }

        public CrmPartyRef build() {
            return CrmPartyRef.this;
        }

    }
}
