package model;

import java.util.Objects;

public class RelatedPartyRef {
    private String id;
    private OriginSourceEnum originSource;
    private Integer matchingQuality;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public OriginSourceEnum getOriginSource() {
        return originSource;
    }

    public void setOriginSource(OriginSourceEnum originSource) {
        this.originSource = originSource;
    }

    public Integer getMatchingQuality() {
        return matchingQuality;
    }

    public void setMatchingQuality(Integer matchingQuality) {
        this.matchingQuality = matchingQuality;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RelatedPartyRef relatedPartyRef = (RelatedPartyRef) o;
        return Objects.equals(this.id, relatedPartyRef.id) &&
                Objects.equals(this.originSource, relatedPartyRef.originSource) &&
                Objects.equals(this.matchingQuality, relatedPartyRef.matchingQuality);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, originSource, matchingQuality);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class model.RelatedPartyRef {\n");
        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    originSource: ").append(toIndentedString(originSource)).append("\n");
        sb.append("    matchingQuality: ").append(toIndentedString(matchingQuality)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }

    public static Builder newBuilder() {
        return new RelatedPartyRef().new Builder();
    }

    public class Builder {

        private Builder() {
        }

        public RelatedPartyRef.Builder setId(String id) {
            RelatedPartyRef.this.id = id;

            return this;
        }

        public RelatedPartyRef.Builder setOriginSource(OriginSourceEnum originSource) {
            RelatedPartyRef.this.originSource = originSource;

            return this;
        }

        public RelatedPartyRef.Builder setMatchingQuality(Integer matchingQuality) {
            RelatedPartyRef.this.matchingQuality = matchingQuality;

            return this;
        }

        public RelatedPartyRef build() {
            return RelatedPartyRef.this;
        }

    }

}
