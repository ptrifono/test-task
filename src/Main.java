import model.*;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        CdpPartyResult partyResult = getPartyResult();

    }

    private static CdpPartyResult getPartyResult() {
        Customer customer1 = Customer.newBuilder()
                .setId("id_1")
                .setOriginSource(OriginSourceEnum.FN)
                .setStatus(StatusEnum.ACTIVE)
                .build();
        Customer customer2 = Customer.newBuilder()
                .setId("id_2")
                .setOriginSource(OriginSourceEnum.MF)
                .setStatus(StatusEnum.ACTIVE)
                .build();
        Customer customer3 = Customer.newBuilder()
                .setId("id_3")
                .setOriginSource(OriginSourceEnum.FN)
                .setStatus(StatusEnum.ACTIVE)
                .build();

        RelatedPartyRef relatedPartyRef1 = RelatedPartyRef.newBuilder()
                .setId("id_4")
                .setMatchingQuality(100)
                .setOriginSource(OriginSourceEnum.MH)
                .build();

        RelatedPartyRef relatedPartyRef2 = RelatedPartyRef.newBuilder()
                .setId("id_5")
                .setMatchingQuality(90)
                .setOriginSource(OriginSourceEnum.FN)
                .build();

        RelatedPartyRef relatedPartyRef3 = RelatedPartyRef.newBuilder()
                .setId("id_6")
                .setMatchingQuality(100)
                .setOriginSource(OriginSourceEnum.FN)
                .build();

        CrmPartyRef crmPartyRef1 = CrmPartyRef.newBuilder()
                .setId("crmId_1")
                .setCustomer(customer1)
                .setPartyRelationships(null)
                .build();

        CrmPartyRef crmPartyRef2 = CrmPartyRef.newBuilder()
                .setId("crmId_2")
                .setCustomer(customer2)
                .setPartyRelationships(List.of(relatedPartyRef1, relatedPartyRef2))
                .build();

        CrmPartyRef crmPartyRef3 = CrmPartyRef.newBuilder()
                .setId("crmId_3")
                .setCustomer(customer3)
                .setPartyRelationships(List.of(relatedPartyRef3))
                .build();

        return CdpPartyResult.newBuilder()
                .setId("id")
                .setCrmPartyRefs(List.of(crmPartyRef1, crmPartyRef2, crmPartyRef3))
                .build();
    }
}